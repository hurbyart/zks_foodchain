package cz.cvut.fel.omo.foodChain.product;

import cz.cvut.fel.omo.foodChain.chainParticipants.Customer;
import cz.cvut.fel.omo.foodChain.chainParticipants.Retailer;
import cz.cvut.fel.omo.foodChain.operations.TransactionInformer;
import cz.cvut.fel.omo.foodChain.states.FinalPackedState;
import cz.cvut.fel.omo.foodChain.states.TemporaryPackedState;
import cz.cvut.fel.omo.foodChain.states.UnpackedState;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static cz.cvut.fel.omo.foodChain.product.FoodEnum.ONION;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;


public class FoodEntityStateMachineTest {

    private FoodEntity foodEntity;

    @BeforeEach
    public void setUp() {
        foodEntity = new FoodEntity(ONION, 1, 5, 40);
    }

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;
    private final PrintStream originalErr = System.err;


    @BeforeEach
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
    }

    @AfterEach
    public void restoreStreams() {
        System.setOut(originalOut);
        System.setErr(originalErr);
    }

    /**
     * After using useForCooking function product state changed to unpacked.
     */
    @Tag("Unit_test")
    @Test
    void useForCooking_stateChangedToUnpacked() {
        // arrange
        foodEntity.setState(new TemporaryPackedState(foodEntity));
        String expectedOutput = "Product ONION cannot be used for cooking until it is packed. It will be unpacked.\n";

        // act
        foodEntity.useForCooking();

        // assert
        assertEquals(UnpackedState.class, foodEntity.getState().getClass());
        assertEquals(expectedOutput, outContent.toString());
    }

    /**
     * After using transport function with Retailer in request product state changed to temporary packed.
     */
    @Tag("Unit_test")
    @Test
    void transportToRetailer_stateChangedToTemporaryPacked() {
        // arrange
        Retailer retailer = new Retailer("Retailer", 1000, null);
        String expectedOutput = "Product ONION cannot be transported until it is not packed. It will be packed in transport packaging.\n";

        // act
        foodEntity.transport(retailer);

        // assert
        assertEquals(TemporaryPackedState.class, foodEntity.getState().getClass());
        assertEquals(expectedOutput, outContent.toString());
    }

    /**
     * After using transport function with Customer in request product state changed to final packed.
     */
    @Tag("Unit_test")
    @Test
    void transportToCustomer_stateChangedToFinalPacked() {
        // arrange
        Customer customer = new Customer("Lukas", 100, new TransactionInformer());
        String expectedOutput = "Product ONION cannot be presented to customer until it is not finally packed. It will be packed in final packaging.\n";

        // act
        foodEntity.transport(customer);

        // assert
        assertEquals(FinalPackedState.class, foodEntity.getState().getClass());
        assertEquals(expectedOutput, outContent.toString());
    }


}