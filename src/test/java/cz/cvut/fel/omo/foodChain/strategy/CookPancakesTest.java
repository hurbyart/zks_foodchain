package cz.cvut.fel.omo.foodChain.strategy;

import cz.cvut.fel.omo.foodChain.chainParticipants.Manufacturer;
import cz.cvut.fel.omo.foodChain.product.FoodEntity;
import cz.cvut.fel.omo.foodChain.product.FoodEnum;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import static cz.cvut.fel.omo.foodChain.product.FoodEnum.*;
import static org.junit.jupiter.api.Assertions.*;

class CookPancakesTest {

    Manufacturer manufacturer;
    CookPancakes cookPancakes;
    Set<FoodEnum> receipt;

    @BeforeEach
    void setUp() {
        this.manufacturer = new Manufacturer("PANCAKES MANUFACTURER", 1000,
                KYIVCUTLET, null);
        this.cookPancakes = new CookPancakes(manufacturer);
        this.receipt = new HashSet<>(Arrays.asList(FLOUR, MILK, SUGAR));
    }


    /**
     * Check if receipt filled in correctly.
     */
    @Tag("Unit_test")
    @Test
    void fillReceipt_ingredientsAreInReceipt() {
        // arrange
        manufacturer.addFood(new FoodEntity(FLOUR, 1, 5, 40));
        manufacturer.addFood(new FoodEntity(MILK, 1, 5, 40));
        manufacturer.addFood(new FoodEntity(SUGAR, 1, 5, 40));


        // act
        cookPancakes.fillReceipt();
        HashMap<FoodEnum, FoodEntity> filledReceipt = cookPancakes.getReceipt();


        for (FoodEnum ingredient : receipt) {
            // assert
            assertNotNull(filledReceipt.get(ingredient));
        }
    }

    /**
     * Check if receipt is not filled in fully, if only some ingredients were added.
     */
    @Tag("Unit_test")
    @Test
    void fillReceiptOnlyPartOfReceipt_receiptIsFilledInPartly() {
        // arrange
        manufacturer.addFood(new FoodEntity(SUGAR, 1, 5, 40));
        boolean somethingIsAbsent = false;


        // act
        cookPancakes.fillReceipt();
        HashMap<FoodEnum, FoodEntity> filledReceipt = cookPancakes.getReceipt();
        for (FoodEnum ingredient : receipt) {
            if (filledReceipt.get(ingredient) == null) somethingIsAbsent = true;
        }

        // assert
        assertTrue(somethingIsAbsent);
    }

    /**
     * Check if ready for cooking when all ingredients are added.
     */
    @Tag("Unit_test")
    @Test
    void isReadyForCookingWithAllIngredients_isReady() {
        // arrange
        manufacturer.addFood(new FoodEntity(FLOUR, 1, 5, 40));
        manufacturer.addFood(new FoodEntity(MILK, 1, 5, 40));
        manufacturer.addFood(new FoodEntity(SUGAR, 1, 5, 40));
        cookPancakes.fillReceipt();

        // act
        boolean ifReadyForCooking = cookPancakes.isReadyForCooking();

        // assert
        assertTrue(ifReadyForCooking);

    }

    /**
     * Check if food is not ready for cooking if not all ingredients are present
     */
    @Tag("Unit_test")
    @Test
    void isNotReadyForCookingWithoutAllIngredients_notReady() {
        // arrange
        manufacturer.addFood(new FoodEntity(FLOUR, 1, 5, 40));
        cookPancakes.fillReceipt();

        // act
        boolean ifReadyForCooking = cookPancakes.isReadyForCooking();

        // assert
        assertFalse(ifReadyForCooking);
    }

    /**
     * Check if cooks borshch from all ingredients.
     */
    @Tag("Unit_test")
    @Test
    void cooksBorshch_borshchReturned() {
        // arrange
        FoodEnum expectedFoodEnum = PANCAKES;
        manufacturer.addFood(new FoodEntity(FLOUR, 1, 5, 40));
        manufacturer.addFood(new FoodEntity(MILK, 2, -5, 40));
        manufacturer.addFood(new FoodEntity(SUGAR, 2, 5, 40));

        // act
        FoodEntity cookedEntity = cookPancakes.cook();

        // assert
        assertEquals(expectedFoodEnum, cookedEntity.getName());
    }

}
