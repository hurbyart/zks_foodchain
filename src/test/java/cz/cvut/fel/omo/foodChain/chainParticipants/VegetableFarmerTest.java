package cz.cvut.fel.omo.foodChain.chainParticipants;

import cz.cvut.fel.omo.foodChain.exceptions.UnexpectedFoodEntityException;
import cz.cvut.fel.omo.foodChain.operations.OperationEnum;
import cz.cvut.fel.omo.foodChain.operations.Request;
import cz.cvut.fel.omo.foodChain.product.FoodEntity;
import cz.cvut.fel.omo.foodChain.product.FoodEnum;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static cz.cvut.fel.omo.foodChain.product.FoodEnum.*;
import static cz.cvut.fel.omo.foodChain.product.FoodEnum.WATER;
import static org.junit.jupiter.api.Assertions.*;

class VegetableFarmerTest {

    Party vegetableFarmer;

    @BeforeEach
    void setUp() {
        List<FoodEnum> vegetableIngredients = Arrays.asList(POTATO, ONION, SUGAR, FLOUR, BEET, WATER);
        vegetableFarmer = new VegetableFarmer("VEGETABLE FARMER", 42, "UKRAINE", 2000,
                vegetableIngredients, null);
    }


    /**
     * Vegetable farmer is requested for one potato.
     * One potato returned.
     */
    @Tag("Unit_test")
    @Test
    void vegetableFarmerAskedForOnePotato_onePotatoReturned() {
        // arrange
        FoodEnum expectedFoodEntity = FoodEnum.POTATO;
        int expectedFoodEntityAmount = 1;
        Request request = new Request(null, expectedFoodEntity, expectedFoodEntityAmount, OperationEnum.GROW);

        // assert
        assertDoesNotThrow(() -> {
            // act
            FoodEntity foodEntity = vegetableFarmer.process(request);

            // assert
            assertEquals(expectedFoodEntity, foodEntity.getName());
            assertEquals(foodEntity.getQuantity(), expectedFoodEntityAmount);
        });
    }

    /**
     * Vegetable farmer is requested for one potato.
     * UnexpectedFoodEntityException is thrown.
     */
    @Tag("Unit_test")
    @Test
    void VegetableFarmerAskedForOneMeat_throwsException() {
        // arrange
        Request request = new Request(null, FoodEnum.MEAT, 1, OperationEnum.GROW);
        String expectedMessage = "Unexpected food entity!";

        // assert
        Exception exception = assertThrows(UnexpectedFoodEntityException.class, () ->
                // act
                vegetableFarmer.process(request)
        );

        assertEquals(expectedMessage, exception.getMessage());
    }

    /**
     * Vegetable farmer is requested for three sugars.
     * Three sugars returned.
     */
    @Tag("Unit_test")
    @Test
    void vegetableFarmerAskedForThreeSugars_threeSugarsReturned() {
        FoodEnum expectedFoodEntity = FoodEnum.SUGAR;
        int expectedFoodEntityAmount = 3;
        Request request = new Request(null, expectedFoodEntity, expectedFoodEntityAmount, OperationEnum.GROW);

        // assert
        assertDoesNotThrow(() -> {
            // act
            FoodEntity foodEntity = vegetableFarmer.process(request);

            // assert
            assertEquals(expectedFoodEntity, foodEntity.getName());
            assertEquals(expectedFoodEntityAmount, foodEntity.getQuantity());
        });
    }


}