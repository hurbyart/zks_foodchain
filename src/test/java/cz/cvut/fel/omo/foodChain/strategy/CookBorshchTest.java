package cz.cvut.fel.omo.foodChain.strategy;

import cz.cvut.fel.omo.foodChain.chainParticipants.Manufacturer;
import cz.cvut.fel.omo.foodChain.product.FoodEntity;
import cz.cvut.fel.omo.foodChain.product.FoodEnum;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import static cz.cvut.fel.omo.foodChain.product.FoodEnum.*;
import static cz.cvut.fel.omo.foodChain.product.FoodEnum.MEAT;
import static org.junit.jupiter.api.Assertions.*;

class CookBorshchTest {

    Manufacturer manufacturer;
    CookBorshch cookBorshch;
    Set<FoodEnum> receipt;

    @BeforeEach
    void setUp() {
        this.manufacturer = new Manufacturer("BORSHCH MANUFACTURER", 1000,
                BORSHCH, null);
        this.cookBorshch = new CookBorshch(manufacturer);
        this.receipt = new HashSet<>(Arrays.asList(BEET, MEAT, POTATO, WATER));
    }


    /**
     * Check if receipt filled in correctly.
     */
    @Tag("Unit_test")
    @Test
    void fillReceipt_ingredientsAreInReceipt() {
        // arrange
        manufacturer.addFood(new FoodEntity(BEET, 1, 5, 40));
        manufacturer.addFood(new FoodEntity(MEAT, 1, -5, 40));
        manufacturer.addFood(new FoodEntity(POTATO, 1, 5, 40));
        manufacturer.addFood(new FoodEntity(WATER, 1, 10, 40));

        // act
        cookBorshch.fillReceipt();
        HashMap<FoodEnum, FoodEntity> filledReceipt = cookBorshch.getReceipt();


        for (FoodEnum ingredient : receipt) {
            // assert
            assertNotNull(filledReceipt.get(ingredient));
        }
    }

    /**
     * Check if receipt is not filled in fully, if only some ingredients were added.
     */
    @Tag("Unit_test")
    @Test
    void fillReceiptOnlyPartOfReceipt_receiptIsFilledInPartly() {
        // arrange
        manufacturer.addFood(new FoodEntity(BEET, 1, 5, 40));
        boolean somethingIsAbsent = false;


        // act
        cookBorshch.fillReceipt();
        HashMap<FoodEnum, FoodEntity> filledReceipt = cookBorshch.getReceipt();
        for (FoodEnum ingredient : receipt) {
            if (filledReceipt.get(ingredient) == null) somethingIsAbsent = true;
        }

        // assert
        assertTrue(somethingIsAbsent);
    }

    /**
     * Check if ready for cooking when all ingredients are added.
     */
    @Tag("Unit_test")
    @Test
    void isReadyForCookingWithAllIngredients_isReady() {
        // arrange
        manufacturer.addFood(new FoodEntity(BEET, 1, 5, 40));
        manufacturer.addFood(new FoodEntity(MEAT, 1, -5, 40));
        manufacturer.addFood(new FoodEntity(POTATO, 1, 5, 40));
        manufacturer.addFood(new FoodEntity(WATER, 1, 10, 40));
        cookBorshch.fillReceipt();

        // act
        boolean ifReadyForCooking = cookBorshch.isReadyForCooking();

        // assert
        assertTrue(ifReadyForCooking);

    }

    /**
     * Check if food is not ready for cooking if not all ingredients are present
     */
    @Tag("Unit_test")
    @Test
    void isNotReadyForCookingWithoutAllIngredients_notReady() {
        // arrange
        manufacturer.addFood(new FoodEntity(BEET, 1, 5, 40));
        cookBorshch.fillReceipt();

        // act
        boolean ifReadyForCooking = cookBorshch.isReadyForCooking();

        // assert
        assertFalse(ifReadyForCooking);
    }

    /**
     * Check if cooks borshch from all ingredients.
     */
    @Tag("Unit_test")
    @Test
    void cooksBorshch_borshchReturned() {
        // arrange
        FoodEnum expectedFoodEnum = BORSHCH;
        manufacturer.addFood(new FoodEntity(BEET, 1, 5, 40));
        manufacturer.addFood(new FoodEntity(MEAT, 2, -5, 40));
        manufacturer.addFood(new FoodEntity(POTATO, 1, 5, 40));
        manufacturer.addFood(new FoodEntity(WATER, 2, 10, 40));

        FoodEntity cookedEntity = cookBorshch.cook();


        // assert
        assertEquals(expectedFoodEnum, cookedEntity.getName());
    }
}