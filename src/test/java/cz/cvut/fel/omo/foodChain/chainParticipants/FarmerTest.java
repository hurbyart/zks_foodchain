package cz.cvut.fel.omo.foodChain.chainParticipants;

import cz.cvut.fel.omo.foodChain.product.FoodEnum;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class FarmerTest {

    /**
     * Parameterized test for Farmer Constructor. The constructor must throw IllegalArgumentException
     * in case of unsuitable argument. Arguments are generated with 2 strength.
     *
     * @param age
     * @param country
     * @param ingredientList
     * @param expectedResult - manually added to arguments file
     */
    @Tag("Parameterized_test")
    @ParameterizedTest
    @CsvFileSource(resources = "/Farmer-output.csv")
    public void constructorThrowsExceptionIfDataIsIllegal(String age, String country, String ingredientList, Integer expectedResult) {
        // arrange
        Integer ageInt;
        if (age.equals("null")) ageInt = null;
        else ageInt = Integer.parseInt(age);

        if (country.equals("null")) country = null;

        List<FoodEnum> ingredientEnumList = new ArrayList<>();
        if (ingredientList.equals("null")) ingredientEnumList = null;
        else {
            String[] ingredientListSeparated = ingredientList.split(";");
            for (String s : ingredientListSeparated) {
                ingredientEnumList.add(FoodEnum.valueOf(s));
            }
        }

        int programResult = 1;

        // act
        try {
            new VegetableFarmer("Ivan", ageInt, country, 1000, ingredientEnumList, null);
        } catch (IllegalArgumentException ex) {
            programResult = 0;
        }

        // assert
        assertEquals(expectedResult, programResult);
    }
}