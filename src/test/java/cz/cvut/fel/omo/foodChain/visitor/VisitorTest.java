package cz.cvut.fel.omo.foodChain.visitor;

import cz.cvut.fel.omo.foodChain.chainParticipants.MeatFarmer;
import cz.cvut.fel.omo.foodChain.chainParticipants.Storehouse;
import cz.cvut.fel.omo.foodChain.chainParticipants.VegetableFarmer;
import cz.cvut.fel.omo.foodChain.product.FoodEnum;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static cz.cvut.fel.omo.foodChain.product.FoodEnum.*;
import static org.junit.jupiter.api.Assertions.*;

class VisitorTest {


    /**
     * Using visitor to fill in Vegetable farmer prices.
     * Prices are filled in.
     */
    @Tag("Unit_test")
    @Test
    void fillInVegetableFarmerPrices_fillsIn() {
        // arrange
        List<FoodEnum> vegetableIngredients = Arrays.asList(POTATO, ONION, SUGAR, FLOUR, BEET, WATER);
        int expectedIngredientsSize = vegetableIngredients.size();
        VegetableFarmer vegetableFarmer = new VegetableFarmer("VEGETABLE FARMER", 35, "BELARUS", 2000,
                vegetableIngredients, null);
        Visitor priceWriter = new PriceWriter();

        // act
        priceWriter.doForVegetableFarmer(vegetableFarmer);
        int mapIngredientsCostSize = vegetableFarmer.getPrices().size();

        // assert
        assertEquals(expectedIngredientsSize, mapIngredientsCostSize);
        for (Map.Entry<FoodEnum, Integer> e : vegetableFarmer.getPrices().entrySet()) {
            assertNotNull(e.getKey());
        }
    }

    /**
     * Using visitor to fill in Meat farmer prices.
     * Prices are filled in.
     */
    @Tag("Unit_test")
    @Test
    void fillInMeatFarmerPrices_fillsIn() {
        // arrange
        List<FoodEnum> meatIngredients = Arrays.asList(MILK, MEAT, BUTTER, EGGS);
        int expectedIngredientsSize = meatIngredients.size();
        MeatFarmer meatFarmer = new MeatFarmer("MEAT FARMER", 35, "BELARUS", 2000,
                meatIngredients, null);
        Visitor priceWriter = new PriceWriter();

        // act
        priceWriter.doForMeatFarmer(meatFarmer);
        int mapIngredientsCostSize = meatFarmer.getPrices().size();

        // assert
        assertEquals(expectedIngredientsSize, mapIngredientsCostSize);
        for (Map.Entry<FoodEnum, Integer> e : meatFarmer.getPrices().entrySet()) {
            assertNotNull(e.getKey());
        }
    }

    /**
     * Using visitor to fill in Storehouse farmer prices.
     * Prices are filled in.
     */
    @Tag("Unit_test")
    @Test
    void fillInStorehousePrices_fillsIn() {
        // arrange
        Storehouse storehouse = new Storehouse("STOREHOUSE", 100, null);
        int expectedMapIngredientsCostSize = 15;
        Visitor priceWriter = new PriceWriter();

        // act
        priceWriter.doForStorehouse(storehouse);
        int realMapIngredientsCostSize = storehouse.getPrices().size();

        // assert
        assertEquals(expectedMapIngredientsCostSize, realMapIngredientsCostSize);
        for (Map.Entry<FoodEnum, Integer> e : storehouse.getPrices().entrySet()) {
            assertNotNull(e.getKey());
        }
    }

}