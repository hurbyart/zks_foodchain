package cz.cvut.fel.omo.foodChain.chainParticipants;

import cz.cvut.fel.omo.foodChain.product.FoodEntity;
import cz.cvut.fel.omo.foodChain.product.FoodEnum;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


class StorehouseTest {

    Storehouse storehouse;

    @BeforeEach
    void setUp() {
        storehouse = new Storehouse("STOREHOUSE", 100, null);
    }

    /**
     * Food with storage temperature 8 is added to storehouse.
     * Food is added to warm fridge;
     */
    @Tag("Unit_test")
    @Test
    void addingFoodWithStorageTemperature8_foodIsAddedToWarmFridge() {
        // arrange
        FoodEntity foodEntity = new FoodEntity(FoodEnum.PANCAKES, 1, 8, 40);

        // act
        storehouse.addFood(foodEntity);

        // assert
        assertTrue(storehouse.getWarmFridge().contains(foodEntity));
        assertFalse(storehouse.getMediumFridge().contains(foodEntity));
        assertFalse(storehouse.getColdFridge().contains(foodEntity));

    }

    /**
     * Food with storage temperature -5 is added to storehouse.
     * Food is added to medium fridge;
     */
    @Tag("Unit_test")
    @Test
    void ddingFoodWithStorageTemperatureMinus5_foodIsAddedToMediumFridge() {
        // arrange
        FoodEntity foodEntity = new FoodEntity(FoodEnum.KYIVCUTLET, 1, -5, 40);

        // act
        storehouse.addFood(foodEntity);

        // assert
        assertTrue(storehouse.getMediumFridge().contains(foodEntity));
        assertFalse(storehouse.getWarmFridge().contains(foodEntity));
        assertFalse(storehouse.getColdFridge().contains(foodEntity));
    }

    /**
     * Food with storage temperature -18 is added to storehouse.
     * Food is added to cold fridge;
     */
    @Tag("Unit_test")
    @Test
    void ddingFoodWithStorageTemperatureMinus18_foodIsAddedToColdFridge() {
        // arrange
        FoodEntity foodEntity = new FoodEntity(FoodEnum.ICECREAM, 1, -18, 40);

        // act
        storehouse.addFood(foodEntity);

        // assert
        assertTrue(storehouse.getColdFridge().contains(foodEntity));
        assertFalse(storehouse.getMediumFridge().contains(foodEntity));
        assertFalse(storehouse.getWarmFridge().contains(foodEntity));
    }
}