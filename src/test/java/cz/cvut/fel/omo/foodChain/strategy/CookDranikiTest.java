package cz.cvut.fel.omo.foodChain.strategy;

import cz.cvut.fel.omo.foodChain.chainParticipants.Manufacturer;
import cz.cvut.fel.omo.foodChain.product.FoodEntity;
import cz.cvut.fel.omo.foodChain.product.FoodEnum;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import static cz.cvut.fel.omo.foodChain.product.FoodEnum.*;
import static org.junit.jupiter.api.Assertions.*;

class CookDranikiTest {

    Manufacturer manufacturer;
    CookDraniki cookDraniki;
    Set<FoodEnum> receipt;

    @BeforeEach
    void setUp() {
        this.manufacturer = new Manufacturer("DRANIKI MANUFACTURER", 1000,
                DRANIKI, null);
        this.cookDraniki = new CookDraniki(manufacturer);
        this.receipt = new HashSet<>(Arrays.asList(POTATO, ONION, FLOUR, EGGS));
    }

    /**
     * Check if receipt filled in correctly.
     */
    @Tag("Unit_test")
    @Test
    void fillReceipt_ingredientsAreInReceipt() {
        // arrange
        manufacturer.addFood(new FoodEntity(POTATO, 1, 5, 40));
        manufacturer.addFood(new FoodEntity(ONION, 1, -5, 40));
        manufacturer.addFood(new FoodEntity(FLOUR, 1, 5, 40));
        manufacturer.addFood(new FoodEntity(EGGS, 1, 10, 40));

        // act
        cookDraniki.fillReceipt();
        HashMap<FoodEnum, FoodEntity> filledReceipt = cookDraniki.getReceipt();


        for (FoodEnum ingredient : receipt) {
            // assert
            assertNotNull(filledReceipt.get(ingredient));
        }
    }

    /**
     * Check if receipt is not filled in fully, if only some ingredients were added.
     */
    @Tag("Unit_test")
    @Test
    void fillReceiptOnlyPartOfReceipt_receiptIsFilledInPartly() {
        // arrange
        manufacturer.addFood(new FoodEntity(POTATO, 1, 5, 40));
        boolean somethingIsAbsent = false;


        // act
        cookDraniki.fillReceipt();
        HashMap<FoodEnum, FoodEntity> filledReceipt = cookDraniki.getReceipt();
        for (FoodEnum ingredient : receipt) {
            if (filledReceipt.get(ingredient) == null) somethingIsAbsent = true;
        }

        // assert
        assertTrue(somethingIsAbsent);
    }

    /**
     * Check if ready for cooking when all ingredients are added.
     */
    @Tag("Unit_test")
    @Test
    void isReadyForCookingWithAllIngredients_isReady() {
        // arrange
        manufacturer.addFood(new FoodEntity(POTATO, 1, 5, 40));
        manufacturer.addFood(new FoodEntity(ONION, 1, -5, 40));
        manufacturer.addFood(new FoodEntity(FLOUR, 1, 5, 40));
        manufacturer.addFood(new FoodEntity(EGGS, 1, 10, 40));
        cookDraniki.fillReceipt();

        // act
        boolean ifReadyForCooking = cookDraniki.isReadyForCooking();

        // assert
        assertTrue(ifReadyForCooking);

    }

    /**
     * Check if food is not ready for cooking if not all ingredients are present
     */
    @Tag("Unit_test")
    @Test
    void isNotReadyForCookingWithoutAllIngredients_notReady() {
        // arrange
        manufacturer.addFood(new FoodEntity(BEET, 1, 5, 40));
        cookDraniki.fillReceipt();

        // act
        boolean ifReadyForCooking = cookDraniki.isReadyForCooking();

        // assert
        assertFalse(ifReadyForCooking);
    }

    /**
     * Check if cooks borshch from all ingredients.
     */
    @Tag("Unit_test")
    @Test
    void cooksBorshch_borshchReturned() {
        // arrange
        FoodEnum expectedFoodEnum = DRANIKI;
        manufacturer.addFood(new FoodEntity(ONION, 1, 5, 40));
        manufacturer.addFood(new FoodEntity(FLOUR, 2, -5, 40));
        manufacturer.addFood(new FoodEntity(POTATO, 1, 5, 40));
        manufacturer.addFood(new FoodEntity(EGGS, 2, 10, 40));

        FoodEntity cookedEntity = cookDraniki.cook();


        // assert
        assertEquals(expectedFoodEnum, cookedEntity.getName());
    }

}