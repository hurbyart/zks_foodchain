package cz.cvut.fel.omo.foodChain.chainParticipants;

import cz.cvut.fel.omo.foodChain.exceptions.UnexpectedRequestSpecializationForManufacturerException;
import cz.cvut.fel.omo.foodChain.operations.OperationEnum;
import cz.cvut.fel.omo.foodChain.operations.Request;
import cz.cvut.fel.omo.foodChain.product.FoodEntity;
import cz.cvut.fel.omo.foodChain.product.FoodEnum;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static cz.cvut.fel.omo.foodChain.product.FoodEnum.*;
import static org.junit.jupiter.api.Assertions.*;

class ManufacturerTest {

    Manufacturer manufacturer;

    @BeforeEach
    void setUp() {
        manufacturer = new Manufacturer("BORSHCH MANUFACTURER", 0, BORSHCH, null);
    }


    /**
     * Onion is requested from manufacturer with BORSHCH specialization.
     * UnexpectedRequestSpecializationForManufacturerException is thrown
     */
    @Tag("Unit_test")
    @Test
    void requestForOnionToManufacturerWithOtherSpecialization_throwsException() {
        // arrange
        Request request = new Request(null, ONION, 1, OperationEnum.PRODUCE);
        String expectedMessage = "Unexpected request specialization for manufacturer!";

        // assert
        Exception exception = assertThrows(UnexpectedRequestSpecializationForManufacturerException.class, () ->
                // act
                manufacturer.process(request)
        );

        assertEquals(expectedMessage, exception.getMessage());
    }

    /**
     * Kyiv cutlet is requested from manufacturer with BORSHCH specialization.
     * UnexpectedRequestSpecializationForManufacturerException is thrown
     */
    @Tag("Unit_test")
    @Test
    void requestForKyivCutletToManufacturerWithOtherSpecialization_throwsException() {
        // arrange
        Request request = new Request(null, KYIVCUTLET, 1, OperationEnum.PRODUCE);
        String expectedMessage = "Unexpected request specialization for manufacturer!";

        // assert
        Exception exception = assertThrows(UnexpectedRequestSpecializationForManufacturerException.class, () ->
                // act
                manufacturer.process(request)
        );

        assertEquals(expectedMessage, exception.getMessage());
    }

    /**
     * BORSHCH is requested from manufacturer with BORSHCH specialization.
     * Manufacturer returns BORSHCH
     */
    @Tag("Unit_test")
    @Test
    void requestForBorshchToManufacturerWithBorshchSpecialization_BorshchReturned() {
        // arrange
        List<FoodEntity> foodEntitiesForBorshch = Arrays.asList(new FoodEntity(BEET, 1, 10, 40),
                new FoodEntity(WATER, 1, 6, 40),
                new FoodEntity(POTATO, 1, 10, 40),
                new FoodEntity(MEAT, 1, 3, 40));
        manufacturer.getProducts().addAll(foodEntitiesForBorshch);
        FoodEnum expectedFoodEntity = BORSHCH;
        int expectedAmount = 1;
        Request request = new Request(null, expectedFoodEntity, expectedAmount, OperationEnum.PRODUCE);

        // assert
        assertDoesNotThrow(() -> {
            // act
            FoodEntity foodEntity = manufacturer.process(request);

            // assert
            assertEquals(expectedFoodEntity, foodEntity.getName());
            assertEquals(expectedAmount, foodEntity.getQuantity());
        });
    }

}