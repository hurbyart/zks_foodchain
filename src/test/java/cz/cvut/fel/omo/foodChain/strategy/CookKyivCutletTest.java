package cz.cvut.fel.omo.foodChain.strategy;

import cz.cvut.fel.omo.foodChain.chainParticipants.Manufacturer;
import cz.cvut.fel.omo.foodChain.product.FoodEntity;
import cz.cvut.fel.omo.foodChain.product.FoodEnum;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import static cz.cvut.fel.omo.foodChain.product.FoodEnum.*;
import static org.junit.jupiter.api.Assertions.*;

class CookKyivCutletTest {

    Manufacturer manufacturer;
    CookKyivCutlet cookKyivCutlet;
    Set<FoodEnum> receipt;

    @BeforeEach
    void setUp() {
        this.manufacturer = new Manufacturer("KYIV CUTLET MANUFACTURER", 1000,
                KYIVCUTLET, null);
        this.cookKyivCutlet = new CookKyivCutlet(manufacturer);
        this.receipt = new HashSet<>(Arrays.asList(MEAT, FLOUR, BUTTER));
    }


    /**
     * Check if receipt filled in correctly.
     */
    @Tag("Unit_test")
    @Test
    void fillReceipt_ingredientsAreInReceipt() {
        // arrange
        manufacturer.addFood(new FoodEntity(FLOUR, 1, 5, 40));
        manufacturer.addFood(new FoodEntity(MEAT, 1, -5, 40));
        manufacturer.addFood(new FoodEntity(BUTTER, 1, -5, 40));


        // act
        cookKyivCutlet.fillReceipt();
        HashMap<FoodEnum, FoodEntity> filledReceipt = cookKyivCutlet.getReceipt();


        for (FoodEnum ingredient : receipt) {
            // assert
            assertNotNull(filledReceipt.get(ingredient));
        }
    }

    /**
     * Check if receipt is not filled in fully, if only some ingredients were added.
     */
    @Tag("Unit_test")
    @Test
    void fillReceiptOnlyPartOfReceipt_receiptIsFilledInPartly() {
        // arrange
        manufacturer.addFood(new FoodEntity(BUTTER, 1, 5, 40));
        boolean somethingIsAbsent = false;


        // act
        cookKyivCutlet.fillReceipt();
        HashMap<FoodEnum, FoodEntity> filledReceipt = cookKyivCutlet.getReceipt();
        for (FoodEnum ingredient : receipt) {
            if (filledReceipt.get(ingredient) == null) somethingIsAbsent = true;
        }

        // assert
        assertTrue(somethingIsAbsent);
    }

    /**
     * Check if ready for cooking when all ingredients are added.
     */
    @Tag("Unit_test")
    @Test
    void isReadyForCookingWithAllIngredients_isReady() {
        // arrange
        manufacturer.addFood(new FoodEntity(FLOUR, 1, 5, 40));
        manufacturer.addFood(new FoodEntity(MEAT, 1, -5, 40));
        manufacturer.addFood(new FoodEntity(BUTTER, 1, -5, 40));
        cookKyivCutlet.fillReceipt();

        // act
        boolean ifReadyForCooking = cookKyivCutlet.isReadyForCooking();

        // assert
        assertTrue(ifReadyForCooking);

    }

    /**
     * Check if food is not ready for cooking if not all ingredients are present
     */
    @Tag("Unit_test")
    @Test
    void isNotReadyForCookingWithoutAllIngredients_notReady() {
        // arrange
        manufacturer.addFood(new FoodEntity(BUTTER, 1, 5, 40));
        cookKyivCutlet.fillReceipt();

        // act
        boolean ifReadyForCooking = cookKyivCutlet.isReadyForCooking();

        // assert
        assertFalse(ifReadyForCooking);
    }

    /**
     * Check if cooks borshch from all ingredients.
     */
    @Tag("Unit_test")
    @Test
    void cooksBorshch_borshchReturned() {
        // arrange
        FoodEnum expectedFoodEnum = KYIVCUTLET;
        manufacturer.addFood(new FoodEntity(FLOUR, 1, 5, 40));
        manufacturer.addFood(new FoodEntity(MEAT, 2, -5, 40));
        manufacturer.addFood(new FoodEntity(BUTTER, 2, -5, 40));

        // act
        FoodEntity cookedEntity = cookKyivCutlet.cook();

        // assert
        assertEquals(expectedFoodEnum, cookedEntity.getName());
    }

}