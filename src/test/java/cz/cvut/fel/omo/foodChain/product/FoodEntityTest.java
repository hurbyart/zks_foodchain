package cz.cvut.fel.omo.foodChain.product;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FoodEntityTest {

    public void runTest(String name, String quantity, String quality, String storageTemperature, Integer expectedResult) {
        // arrange
        FoodEnum nameFoodEnum;
        if (name.equals("null")) nameFoodEnum = null;
        else nameFoodEnum = FoodEnum.valueOf(name);

        Integer quantityInt;
        if (quantity.equals("null")) quantityInt = null;
        else quantityInt = Integer.valueOf(quantity);

        Integer qualityInt;
        if (quality.equals("null")) qualityInt = null;
        else qualityInt = Integer.valueOf(quality);

        Integer storageTemperatureInt;
        if (storageTemperature.equals("null")) storageTemperatureInt = null;
        else storageTemperatureInt = Integer.valueOf(storageTemperature);
        int programResult = 1;

        // act
        try {
            new FoodEntity(nameFoodEnum, quantityInt, storageTemperatureInt, qualityInt);
        } catch (IllegalArgumentException ex) {
            programResult = 0;
        }
        // assert
        assertEquals(expectedResult, programResult);
    }

    /**
     * Parameterized test for FoodEntity Constructor. The constructor must throw IllegalArgumentException
     * in case of unsuitable argument. Arguments are generated with 3 strength.
     *
     * @param name
     * @param quantity
     * @param quality
     * @param storageTemperature
     * @param expectedResult     - manually added to arguments file
     */
    @Tag("Parameterized_test")
    @ParameterizedTest
    @CsvFileSource(resources = "/Food-Entity-output3.csv")
    public void constructorThrowsExceptionIfDataIsIllegalStrength3(String name, String quantity, String quality, String storageTemperature, Integer expectedResult) {
        runTest(name, quantity, quality, storageTemperature, expectedResult);
    }

    /**
     * Parameterized test for FoodEntity Constructor. The constructor must throw IllegalArgumentException
     * in case of unsuitable argument. Arguments are generated with 2 strength.
     *
     * @param name
     * @param quantity
     * @param quality
     * @param storageTemperature
     * @param expectedResult     - manually added to arguments file
     */
    @Tag("Parameterized_test")
    @ParameterizedTest
    @CsvFileSource(resources = "/Food-Entity-output2.csv")
    public void constructorThrowsExceptionIfDataIsIllegalStrength2(String name, String quantity, String quality, String storageTemperature, Integer expectedResult) {
        runTest(name, quantity, quality, storageTemperature, expectedResult);
    }

}
