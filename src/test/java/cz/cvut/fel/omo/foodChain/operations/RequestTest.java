package cz.cvut.fel.omo.foodChain.operations;

import cz.cvut.fel.omo.foodChain.product.FoodEntity;
import cz.cvut.fel.omo.foodChain.product.FoodEnum;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;

import static org.junit.jupiter.api.Assertions.*;

class RequestTest {

    /**
     * Parameterized test for Request Constructor. The constructor must throw IllegalArgumentException
     * in case of unsuitable argument. Arguments are generated with 2 strength.
     *
     * @param food
     * @param quantity
     * @param operationType
     * @param expectedResult - manually added to arguments file
     */
    @Tag("Parameterized_test")
    @ParameterizedTest
    @CsvFileSource(resources = "/Request-output.csv")
    public void constructorThrowsExceptionIfDataIsIllegal(String food, String quantity, String operationType, Integer expectedResult) {
        // arrange
        FoodEnum foodEnum;
        if (food.equals("null")) foodEnum = null;
        else foodEnum = FoodEnum.valueOf(food);

        Integer quantityInt;
        if (quantity.equals("null")) quantityInt = null;
        else quantityInt = Integer.valueOf(quantity);

        OperationEnum operationEnum;
        if (operationType.equals("null")) operationEnum = null;
        else operationEnum = OperationEnum.valueOf(operationType);


        int programResult = 1;

        // act
        try {
            new Request(null, foodEnum, quantityInt, operationEnum);
        } catch (IllegalArgumentException ex) {
            programResult = 0;
        }
        // assert
        assertEquals(expectedResult, programResult);
    }


}