package cz.cvut.fel.omo.foodChain.chainParticipants;

import cz.cvut.fel.omo.foodChain.exceptions.UnexpectedFoodEntityException;
import cz.cvut.fel.omo.foodChain.operations.OperationEnum;
import cz.cvut.fel.omo.foodChain.operations.Request;
import cz.cvut.fel.omo.foodChain.product.FoodEntity;
import cz.cvut.fel.omo.foodChain.product.FoodEnum;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static cz.cvut.fel.omo.foodChain.product.FoodEnum.*;
import static cz.cvut.fel.omo.foodChain.product.FoodEnum.EGGS;
import static org.junit.jupiter.api.Assertions.*;

class MeatFarmerTest {

    Party meatFarmer;

    @BeforeEach
    void setUp() {
        List<FoodEnum> meatIngredients = Arrays.asList(MILK, MEAT, BUTTER, EGGS);
        meatFarmer = new MeatFarmer("MEAT FARMER", 35, "BELARUS", 2000,
                meatIngredients, null);
    }

    /**
     * Request for one meat to meat farmer. Meat farmer returns one meat.
     */
    @Tag("Unit_test")
    @Test
    void oneMeatIsRequestedFromMeatFarmer_returnsMeat() {
        // arrange
        FoodEnum expectedFoodEntity = FoodEnum.MEAT;
        int expectedFoodEntityAmount = 1;
        Request request = new Request(null, expectedFoodEntity, expectedFoodEntityAmount, OperationEnum.GROW);

        // assert
        assertDoesNotThrow(() -> {
            // act
            FoodEntity foodEntity = meatFarmer.process(request);

            // assert
            assertEquals(expectedFoodEntity, foodEntity.getName());
            assertEquals(foodEntity.getQuantity(), expectedFoodEntityAmount);
        });
    }

    /**
     * Request for potato to meat farmer.
     * UnexpectedFoodEntityException is thrown.
     */
    @Tag("Unit_test")
    @Test
    void potatoIsRequestedFromMeatFarmer_throwsException() {
        // arrange
        Request request = new Request(null, POTATO, 1, OperationEnum.GROW);
        String expectedMessage = "Unexpected food entity!";

        // assert
        Exception exception = assertThrows(UnexpectedFoodEntityException.class, () ->
                // act
                meatFarmer.process(request)
        );

        assertEquals(expectedMessage, exception.getMessage());
    }

    /**
     * Request for three milks to meat farmer. Meat farmer returns the milks.
     */
    @Tag("Unit_test")
    @Test
    void threeMilkIsRequestedFromMeatFarmer_returnsMeat() {
        FoodEnum expectedFoodEntity = MILK;
        int expectedFoodEntityAmount = 3;
        Request request = new Request(null, expectedFoodEntity, expectedFoodEntityAmount, OperationEnum.GROW);

        // assert
        assertDoesNotThrow(() -> {
            // act
            FoodEntity foodEntity = meatFarmer.process(request);

            // assert
            assertEquals(expectedFoodEntity, foodEntity.getName());
            assertEquals(foodEntity.getQuantity(), expectedFoodEntityAmount);
        });
    }
}