package cz.cvut.fel.omo.foodChain.chainParticipants;

import cz.cvut.fel.omo.foodChain.operations.OperationEnum;
import cz.cvut.fel.omo.foodChain.operations.Request;
import cz.cvut.fel.omo.foodChain.product.FoodEntity;
import cz.cvut.fel.omo.foodChain.product.FoodEnum;
import cz.cvut.fel.omo.foodChain.visitor.PriceWriter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.*;

class RetailerTest {

    private Retailer retailer;
    private Request request;

    @BeforeEach
    void setUp() {
        retailer = new Retailer("seller", 500, null);
        request = new Request(null, FoodEnum.BORSHCH, 2, OperationEnum.SELL);
    }

    /**
     * Retailer pancake price is filled in with PriceWriter visitor.
     */
    @Tag("Unit_test")
    @Test
    void retailerPancakePriceIsFilledIn_2800() {
        // arrange
        int expectedPrice = 2800;
        retailer.accept(new PriceWriter());
        // act
        int actualPrice = retailer.askPrice(FoodEnum.PANCAKES, 10);
        // assert
        assertEquals(expectedPrice, actualPrice);
    }

    /**
     * BORSHCH is added to retailer foods.
     * Retailer agrees to execute request with BORHSHCH.
     */
    @Tag("Unit_test")
    @Test
    void retailerHasBorshch_agreesToExecuteBorshchRequest() {
        // arrange
        retailer.addFood(new FoodEntity(FoodEnum.BORSHCH, 2, 5, 40));
        // act
        boolean agreeToExecute = retailer.isAgreeToExecute(request);
        // assert
        assertTrue(agreeToExecute);
    }

    /**
     * Retailer has no food.
     * Retailer doesn't agree to execute request with BORHSHCH.
     */
    @Tag("Unit_test")
    @Test
    void retailerHasNoFood_doesntAgreeToExecuteBorshchRequest() {
        // act
        boolean agreeToExecute = retailer.isAgreeToExecute(request);
        // assert
        assertFalse(agreeToExecute);
    }

    /**
     * Retailer has PANCAKE.
     * Retailer doesn't agree to execute request with BORHSHCH.
     */
    @Tag("Unit_test")
    @Test
    void retailerHasPancake_doesntAgreeToExecuteRequest() {
        // arrange
        retailer.addFood(new FoodEntity(FoodEnum.PANCAKES, 2, 5, 40));
        // act
        boolean agreeToExecute = retailer.isAgreeToExecute(request);
        // assert
        assertFalse(agreeToExecute);
    }


    /**
     * Retailer has BORSHCH.
     * Retailer executes request with BORHSHCH and returns BORSHCH.
     */
    @Tag("Unit_test")
    @Test
    void retailerHasBorshch_executesBorshchRequest() {
        // arrange
        FoodEntity expectedProduct = new FoodEntity(FoodEnum.BORSHCH, 1, 5, 40);
        retailer.addFood(expectedProduct);
        // act
        FoodEntity actualProduct = retailer.process(request);
        // assert
        assertEquals(expectedProduct, actualProduct);
    }
}