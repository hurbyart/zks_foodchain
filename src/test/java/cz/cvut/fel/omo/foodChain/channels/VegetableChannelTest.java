package cz.cvut.fel.omo.foodChain.channels;

import cz.cvut.fel.omo.foodChain.chainParticipants.Customer;

import cz.cvut.fel.omo.foodChain.chainParticipants.Party;
import cz.cvut.fel.omo.foodChain.chainParticipants.VegetableFarmer;
import cz.cvut.fel.omo.foodChain.product.FoodEnum;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Tag;


import java.util.Arrays;
import java.util.List;

import static cz.cvut.fel.omo.foodChain.product.FoodEnum.*;
import static cz.cvut.fel.omo.foodChain.product.FoodEnum.WATER;
import static org.junit.jupiter.api.Assertions.*;

public class VegetableChannelTest {
    List<FoodEnum> vegetableIngredients;
    VegetableFarmer vegetableFarmer;
    Customer customer1;
    Customer customer2;
    List<Party> participants;

    @Before
    public void setUp() {
        this.vegetableIngredients = Arrays.asList(POTATO, ONION, SUGAR, FLOUR, BEET, WATER);
        this.vegetableFarmer = new VegetableFarmer("Tomas", 21, "Italy", 4000, vegetableIngredients, null);
        this.customer1 = new Customer("Nick", 500, null);
        this.customer2 = new Customer("Tom", 0, null);
        this.participants = Arrays.asList(vegetableFarmer, customer1, customer2);
    }

    /**
     * Register some parties to channel.
     * Checks if parties are registered.
     */
    @Tag("Unit_test")
    @Test
    public void checkIfPartiesAreRegistered_registered() {
        // arrange
        int expectedParticipantSize = 2;
        VegetableChannel.getVegetableChannel(participants, vegetableIngredients, null);
        VegetableChannel.getVegetableChannel().register(vegetableFarmer);
        VegetableChannel.getVegetableChannel().register(customer2);

        // act
        List<Party> registeredParticipants = VegetableChannel.getVegetableChannel().getCurrentParticipants();

        // assert
        assertEquals(expectedParticipantSize, registeredParticipants.size());
        assertTrue(registeredParticipants.contains(vegetableFarmer));
        assertTrue(registeredParticipants.contains(customer2));

    }

    /**
     * Checks if parties was unregistered correctly.
     */
    @Tag("Unit_test")
    @Test
    public void checkIfPartiesAreUnregisteredCorrectly_unregisteredCorrectly() {
        // arrange
        VegetableChannel.getVegetableChannel(participants, vegetableIngredients, null);
        VegetableChannel.getVegetableChannel().register(vegetableFarmer);
        VegetableChannel.getVegetableChannel().register(customer1);


        // act
        boolean unregisterRes1 = VegetableChannel.getVegetableChannel().unregister(vegetableFarmer);
        boolean unregisterRes2 = VegetableChannel.getVegetableChannel().unregister(customer1);

        // assert
        assertTrue(unregisterRes1);
        assertTrue(unregisterRes2);
        assertEquals(0, VegetableChannel.getVegetableChannel().getCurrentParticipants().size());
    }

}
