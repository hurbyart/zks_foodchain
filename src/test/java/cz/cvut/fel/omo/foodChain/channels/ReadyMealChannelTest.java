package cz.cvut.fel.omo.foodChain.channels;

import cz.cvut.fel.omo.foodChain.chainParticipants.*;
import cz.cvut.fel.omo.foodChain.product.FoodEnum;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Tag;

import java.util.Arrays;
import java.util.List;

import static cz.cvut.fel.omo.foodChain.product.FoodEnum.*;
import static org.junit.jupiter.api.Assertions.*;

public class ReadyMealChannelTest {

    List<FoodEnum> readyMeals = Arrays.asList(PANCAKES, KYIVCUTLET, DRANIKI, ICECREAM, BORSHCH);

    Storehouse storehouse;
    Customer customer1;
    Customer customer2;
    List<Party> participants;


    @Before
    public void setUp() {
        storehouse = new Storehouse("Storehouse", 2000, null);
        storehouse.put(BORSHCH, 200);
        customer1 = new Customer("Freddy", 500, null);
        customer2 = new Customer("Kurt", 0, null);
        participants = Arrays.asList(storehouse, customer1, customer2);
    }


    /**
     * Register some parties to channel.
     * Checks if parties are registered.
     */
    @Tag("Unit_test")
    @Test
    public void checkIfPartiesAreRegistered_registered() {
        // arrange
        int expectedParticipantSize = 2;
        ReadyMealChannel.getReadyMealChannel(participants, readyMeals, null);
        ReadyMealChannel.getReadyMealChannel().register(customer2);
        ReadyMealChannel.getReadyMealChannel().register(storehouse);

        // act
        List<Party> registeredParticipants = ReadyMealChannel.getReadyMealChannel().getCurrentParticipants();

        // assert
        assertEquals(expectedParticipantSize, registeredParticipants.size());
        assertEquals(customer2, registeredParticipants.get(0));
        assertEquals(storehouse, registeredParticipants.get(1));

    }

    /**
     * Checks if parties was unregistered correctly.
     */
    @Tag("Unit_test")
    @Test
    public void checkIfPartiesAreUnregisteredCorrectly_unregisteredCorrectly() {
        // arrange
        ReadyMealChannel.getReadyMealChannel(participants, readyMeals, null);
        ReadyMealChannel.getReadyMealChannel().register(customer1);
        ReadyMealChannel.getReadyMealChannel().register(storehouse);


        // act
        boolean unregisterRes1 = ReadyMealChannel.getReadyMealChannel().unregister(customer1);
        boolean unregisterRes2 = ReadyMealChannel.getReadyMealChannel().unregister(storehouse);

        // assert
        assertTrue(unregisterRes1);
        assertTrue(unregisterRes2);
        assertEquals(0, ReadyMealChannel.getReadyMealChannel().getCurrentParticipants().size());
    }

    /**
     * Checks that parties can't be unregistered when they are involved in a transaction.
     */
    @Tag("Unit_test")
    @Test
    public void checkIfPartiesAreNotUnregisteredIfTheyInTransaction_partiesAreNotUnregistered() {
        // arrange
        ReadyMealChannel.getReadyMealChannel(participants, readyMeals, null);
        ReadyMealChannel.getReadyMealChannel().register(customer1);
        ReadyMealChannel.getReadyMealChannel().register(storehouse);
        ReadyMealChannel.getReadyMealChannel().setApplicant(customer1);
        ReadyMealChannel.getReadyMealChannel().setExecutor(storehouse);

        // act
        boolean unregisterRes1 = ReadyMealChannel.getReadyMealChannel().unregister(customer1);
        boolean unregisterRes2 = ReadyMealChannel.getReadyMealChannel().unregister(storehouse);

        // assert
        assertFalse(unregisterRes1);
        assertFalse(unregisterRes2);
        assertEquals(2, ReadyMealChannel.getReadyMealChannel().getCurrentParticipants().size());
    }
}
