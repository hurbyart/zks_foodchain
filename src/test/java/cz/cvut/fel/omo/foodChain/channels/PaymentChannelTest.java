package cz.cvut.fel.omo.foodChain.channels;

import cz.cvut.fel.omo.foodChain.chainParticipants.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Tag;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static cz.cvut.fel.omo.foodChain.product.FoodEnum.*;
import static org.junit.jupiter.api.Assertions.*;


public class PaymentChannelTest {

    Storehouse storehouse;
    Seller seller;
    List<Party> participants;


    @Before
    public void setUp() {
        this.storehouse = new Storehouse("Storehouse", 2000, null);
        this.storehouse.put(BORSHCH, 200);
        this.seller = new Seller("Freddy", 500, null);
        this.participants = Arrays.asList(storehouse, seller);
    }

    /**
     * Checks if parties was unregistered correctly.
     */
    @Tag("Unit_test")
    @Test
    public void checkIfPartiesAreUnregisteredCorrectly_unregisteredCorrectly() {
        // arrange
        PaymentChannel.getPaymentChannel(participants, null);
        PaymentChannel.getPaymentChannel().register(storehouse);
        PaymentChannel.getPaymentChannel().register(seller);

        // act
        boolean unregisterRes1 = PaymentChannel.getPaymentChannel().unregister(seller);
        boolean unregisterRes2 = PaymentChannel.getPaymentChannel().unregister(storehouse);

        // assert
        assertTrue(unregisterRes1);
        assertTrue(unregisterRes2);
        assertEquals(0, ReadyMealChannel.getReadyMealChannel().getCurrentParticipants().size());
    }

    /**
     * Checks that parties can't be unregistered when they are involved in a transaction.
     */
    @Tag("Unit_test")
    @Test
    public void checkIfPartiesAreNotUnregisteredIfTheyInTransaction_partiesAreNotUnregistered() {
        // arrange
        PaymentChannel.getPaymentChannel(participants, null);
        PaymentChannel.getPaymentChannel().register(storehouse);
        PaymentChannel.getPaymentChannel().register(seller);
        PaymentChannel.getPaymentChannel().setApplicant(storehouse);


        // act
        boolean unregisterRes1 = PaymentChannel.getPaymentChannel().unregister(seller);
        boolean unregisterRes2 = PaymentChannel.getPaymentChannel().unregister(storehouse);

        // assert
        assertTrue(unregisterRes1);
        assertFalse(unregisterRes2);
        assertEquals(1, PaymentChannel.getPaymentChannel().getCurrentParticipants().size());
    }
}
