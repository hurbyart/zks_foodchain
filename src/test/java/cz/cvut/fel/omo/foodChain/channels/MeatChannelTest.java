package cz.cvut.fel.omo.foodChain.channels;

import cz.cvut.fel.omo.foodChain.chainParticipants.MeatFarmer;
import cz.cvut.fel.omo.foodChain.chainParticipants.VegetableFarmer;
import cz.cvut.fel.omo.foodChain.product.FoodEnum;
import cz.cvut.fel.omo.foodChain.chainParticipants.Party;
import cz.cvut.fel.omo.foodChain.operations.Request;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Tag;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static cz.cvut.fel.omo.foodChain.product.FoodEnum.*;
import static org.junit.jupiter.api.Assertions.*;

public class MeatChannelTest {

    List<FoodEnum> meatIngredients;
    List<FoodEnum> vegetableIngredients;


    @Before
    public void setUp() {
        this.meatIngredients = Arrays.asList(MILK, MEAT, BUTTER, EGGS);
        this.vegetableIngredients = Arrays.asList(POTATO, ONION, SUGAR, FLOUR, BEET, WATER);
    }


    /**
     * Check if party is registered correctly.
     */
    @Tag("Unit_test")
    @Test
    public void registerParty_partyIsRegistered() {
        MeatFarmer meatFarmer = new MeatFarmer("Lukas", 49, "Czech Republic", 3000, meatIngredients, null);

        List<Party> participants = new ArrayList<>();
        Collections.addAll(participants, meatFarmer);
        MeatChannel.getMeatChannel(participants, meatIngredients, null).register(meatFarmer);

        assertEquals(1, MeatChannel.getMeatChannel().getCurrentParticipants().size());
        assertTrue(MeatChannel.getMeatChannel().getCurrentParticipants().contains(meatFarmer));
    }

    /**
     * Check if executor is found.
     */
    @Tag("Unit_test")
    @Test
    public void findExecutor_executorIsFount() {
        // arrange
        VegetableFarmer vegetableFarmer = new VegetableFarmer("Jiri", 54, "Germany", 2000, vegetableIngredients, null);
        MeatFarmer meatFarmer = new MeatFarmer("Lukas", 49, "Czech Republic", 3000, meatIngredients, null);

        List<Party> participants = new ArrayList<>();
        Collections.addAll(participants, meatFarmer, vegetableFarmer);
        MeatChannel.getMeatChannel(participants, meatIngredients, null).register(meatFarmer);
        Request request = new Request(meatFarmer, MILK, 2, meatFarmer.getOperationType());

        // act
        Party executor = MeatChannel.getMeatChannel().findExecutor(request);

        // assert
        assertNotNull(executor);
    }

    /**
     * Channel doesn't register party which is absent in trusted participants list.
     */
    @Tag("Unit_test")
    @Test
    public void doesntRegisterUntrustedParty_doesntRegister() {
        VegetableFarmer vegetableFarmer = new VegetableFarmer("Jiri", 54, "Germany", 2000, vegetableIngredients, null);
        MeatFarmer meatFarmer = new MeatFarmer("Lukas", 49, "Czech Republic", 3000, meatIngredients, null);
        List<Party> participants = List.of(meatFarmer);
        MeatChannel.getMeatChannel(participants, meatIngredients, null).register(vegetableFarmer);

        assertFalse(MeatChannel.getMeatChannel().getCurrentParticipants().contains(vegetableFarmer));
    }


    /**
     * Checks if executor for milk request is not found when meat farmer is not in channel.
     */
    @Tag("Unit_test")
    @Test
    public void doesntExecuteMilkRequestWithoutMeatFarmerInChannel_doesntRegister() {
        // arrange
        VegetableFarmer vegetableFarmer = new VegetableFarmer("Jiri", 54, "Germany", 2000, vegetableIngredients, null);
        MeatFarmer meatFarmer = new MeatFarmer("Lukas", 49, "Czech Republic", 3000, meatIngredients, null);

        List<Party> participants = List.of(vegetableFarmer);
        MeatChannel.getMeatChannel(participants, meatIngredients, null).register(meatFarmer);
        Request request = new Request(meatFarmer, MILK, 2, meatFarmer.getOperationType());

        // act
        Party executor = MeatChannel.getMeatChannel().findExecutor(request);

        // assert
        assertNull(executor);
    }

}