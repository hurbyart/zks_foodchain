package cz.cvut.fel.omo.foodChain.chainParticipants;

import cz.cvut.fel.omo.foodChain.product.FoodEntity;
import cz.cvut.fel.omo.foodChain.product.FoodEnum;
import cz.cvut.fel.omo.foodChain.chainParticipants.Seller;
import cz.cvut.fel.omo.foodChain.operations.OperationEnum;
import cz.cvut.fel.omo.foodChain.operations.Request;
import cz.cvut.fel.omo.foodChain.visitor.PriceWriter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class SellerTest {

    private Seller seller;
    private Request request;

    @BeforeEach
    void setUp() {
        seller = new Seller("seller", 500, null);
        request = new Request(null, FoodEnum.MILK, 1, OperationEnum.SELL);
    }

    /**
     * Seller Kyiv cutlet price is filled in with PriceWriter visitor.
     */
    @Tag("Unit_test")
    @Test
    void sellerKyivCutletPriceIsFilledIn_3000() {
        // arrange
        int expectedPrice = 3000;
        seller.accept(new PriceWriter());
        // act
        int actualPrice = seller.askPrice(FoodEnum.KYIVCUTLET, 10);
        // assert
        assertEquals(expectedPrice, actualPrice);
    }

    /**
     * MILK is added to seller foods.
     * Seller agrees to execute request with MILK.
     */
    @Tag("Unit_test")
    @Test
    void sellerHasMilk_agreesToExecuteMilkRequest() {
        // arrange
        seller.addFood(new FoodEntity(FoodEnum.MILK, 1, 5, 40));
        // act
        boolean agreeToExecute = seller.isAgreeToExecute(request);
        // assert
        assertTrue(agreeToExecute);
    }

    /**
     * Seller has no food.
     * Seller doesn't agree to execute request with MiLK.
     */
    @Tag("Unit_test")
    @Test
    void sellerHasNoFood_doesntAgreeToExecuteMilkRequest() {
        // act
        boolean agreeToExecute = seller.isAgreeToExecute(request);
        // assert
        assertFalse(agreeToExecute);
    }

    /**
     * Retailer has PANCAKE.
     * Retailer doesn't agree to execute request with BORHSHCH.
     */
    @Tag("Unit_test")
    @Test
    void sellerHasIceCream_doesntAgreeToExecuteRequest() {
        // arrange
        seller.addFood(new FoodEntity(FoodEnum.ICECREAM, 1, 5, 40));
        // act
        boolean agreeToExecute = seller.isAgreeToExecute(request);
        // assert
        assertFalse(agreeToExecute);
    }

    /**
     * Seller has MILK.
     * Seller executes request with MILK and returns MILK.
     */
    @Tag("Unit_test")
    @Test
    void sellerHasMilk_executesMilkRequest() {
        // arrange
        FoodEntity expectedProduct = new FoodEntity(FoodEnum.MILK, 1, 5, 40);
        seller.addFood(expectedProduct);
        // act
        FoodEntity actualProduct = seller.process(request);
        // assert
        assertEquals(expectedProduct, actualProduct);
    }

}