package cz.cvut.fel.omo.foodChain.operations;

import cz.cvut.fel.omo.foodChain.chainParticipants.Party;

public interface Observable {

    void attach(Party party);

    void detach(Party party);

    void notify(Transaction transaction);
}
