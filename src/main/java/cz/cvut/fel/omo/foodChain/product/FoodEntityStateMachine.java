package cz.cvut.fel.omo.foodChain.product;

import cz.cvut.fel.omo.foodChain.chainParticipants.Customer;
import cz.cvut.fel.omo.foodChain.chainParticipants.Party;
import cz.cvut.fel.omo.foodChain.states.FinalPackedState;
import cz.cvut.fel.omo.foodChain.states.State;
import cz.cvut.fel.omo.foodChain.states.TemporaryPackedState;
import cz.cvut.fel.omo.foodChain.states.UnpackedState;

public class FoodEntityStateMachine {

    private State state;
    private FoodEntity foodEntity;

    public FoodEntityStateMachine(FoodEntity foodEntity){
        this.foodEntity = foodEntity;
        this.state = new UnpackedState(foodEntity);
    }

    /**
     * uses for change food entity state to unpacked
     */
    public void useForCooking() {
        if (!state.cook()) state = new UnpackedState(foodEntity);
    }

    /**
     * if applicant is customer food entity will be packed in final packing,otherwise in temporary packing
     *
     * @param applicant
     */
    public void transport(Party applicant) {
        if (applicant instanceof Customer) {
            if (!state.presentProductToCustomer()) state = new FinalPackedState(foodEntity);
        } else if (!state.transport()) state = new TemporaryPackedState(foodEntity);
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }
}
