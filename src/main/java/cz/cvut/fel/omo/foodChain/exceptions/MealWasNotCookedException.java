package cz.cvut.fel.omo.foodChain.exceptions;

public class MealWasNotCookedException extends Exception{

    public MealWasNotCookedException() {
        super("Meal was not cooked!");
    }

}

