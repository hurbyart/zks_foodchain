package cz.cvut.fel.omo.foodChain.exceptions;

public class IllegalArgumentException extends Exception {

    public IllegalArgumentException(String msg) {
        super(msg);
    }
}
