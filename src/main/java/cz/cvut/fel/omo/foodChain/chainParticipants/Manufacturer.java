package cz.cvut.fel.omo.foodChain.chainParticipants;

import cz.cvut.fel.omo.foodChain.exceptions.MealWasNotCookedException;
import cz.cvut.fel.omo.foodChain.exceptions.UnexpectedFoodEntityException;
import cz.cvut.fel.omo.foodChain.exceptions.UnexpectedRequestSpecializationForManufacturerException;
import cz.cvut.fel.omo.foodChain.visitor.Visitor;
import cz.cvut.fel.omo.foodChain.operations.TransactionInformer;
import cz.cvut.fel.omo.foodChain.channels.*;
import cz.cvut.fel.omo.foodChain.operations.*;
import cz.cvut.fel.omo.foodChain.strategy.*;
import cz.cvut.fel.omo.foodChain.product.FoodEntity;
import cz.cvut.fel.omo.foodChain.product.FoodEnum;

import java.util.ArrayList;
import java.util.List;

import static cz.cvut.fel.omo.foodChain.simulation.Main.LOGGER;
import static cz.cvut.fel.omo.foodChain.operations.OperationEnum.*;

/**
 * The class describes the behavior of a manufacturer capable of creating certain types
 * of products based on requests and the availability of available ingredients.
 * If one or another ingredient is missing,
 * the manufacturer makes a request for this type of ingredient to the farmer.
 *
 * @author Artem Hurbych, Pavel Paklonski
 */
public class Manufacturer extends ChainParticipant {

    private ManufacturerStrategy strategy;
    private FoodEnum specialization;
    private List<FoodEntity> products = new ArrayList<>();
    private OperationEnum operationType = PRODUCE;

    public Manufacturer(String name, int cashAccount, FoodEnum specialization, TransactionInformer transactionInformer) {
        super(name, cashAccount, transactionInformer);
        this.specialization = specialization;
    }

    public List<FoodEntity> getProducts() {
        return products;
    }

    public void accept(Visitor visitor) {
        visitor.doForManufacturer(this);
    }

    /**
     * The method registers a chain participant in each of the channels.
     */
    public void registerToTheChannel() {
        MeatChannel.getMeatChannel().register(this);
        VegetableChannel.getVegetableChannel().register(this);
        ReadyMealChannel.getReadyMealChannel().register(this);
        PaymentChannel.getPaymentChannel().register(this);
    }

    @Override
    public OperationEnum getOperationType() {
        return operationType;
    }

    /**
     * The method returns 'true' if the object of the request is included in its specialization.
     *
     * @param request the request
     * @return true if the manufacturer can execute the request
     */
    public boolean isAgreeToExecute(Request request) {
        return request.getFoodEnum().equals(this.specialization);
    }

    private void setStrategy(FoodEnum expectedProduct) throws UnexpectedFoodEntityException {
        switch (expectedProduct) {
            case BORSHCH:
                this.strategy = new CookBorshch(this);
                break;
            case PANCAKES:
                this.strategy = new CookPancakes(this);
                break;
            case KYIVCUTLET:
                this.strategy = new CookKyivCutlet(this);
                break;
            case ICECREAM:
                this.strategy = new CookIceCream(this);
                break;
            case DRANIKI:
                this.strategy = new CookDraniki(this);
                break;
            default:
                throw new UnexpectedFoodEntityException();
        }
    }

    /**
     * Starts the product manufacturing process.
     *
     * @param request the request
     * @return the product
     */
    public FoodEntity process(Request request) throws UnexpectedFoodEntityException, MealWasNotCookedException, UnexpectedRequestSpecializationForManufacturerException {
        if(specialization != request.getFoodEnum()) throw new UnexpectedRequestSpecializationForManufacturerException();
        setStrategy(request.getFoodEnum());
        FoodEntity product = strategy.cook();
        if (product != null) {
            request.done = true;
            product.transport(request.getApplicant());
        }
        else {
            LOGGER.warning("Meal was not created by Manufacturer " + getName());
            throw new MealWasNotCookedException();
        }
        return product;
    }

    /**
     * Add the entity of food to the list of products.
     *
     * @param foodEntity the instance of food that was produced by someone
     */
    public void addFood(FoodEntity foodEntity) {
        foodEntity.addAction(foodEntity + " with name " + foodEntity.getName() + " come to " + this);
        products.add(foodEntity);
        //showProducts();
    }

    public void showProducts(){
        System.out.println("RETAILER PRODUCTS:");
        for (FoodEntity product:products) {
            System.out.println(product.getName() + " " + product.getQuantity());
        }
    }
}