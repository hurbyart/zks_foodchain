package cz.cvut.fel.omo.foodChain.chainParticipants;

import cz.cvut.fel.omo.foodChain.exceptions.MealWasNotCookedException;
import cz.cvut.fel.omo.foodChain.exceptions.UnexpectedFoodEntityException;
import cz.cvut.fel.omo.foodChain.product.FoodEntity;
import cz.cvut.fel.omo.foodChain.product.FoodEnum;
import cz.cvut.fel.omo.foodChain.operations.TransactionInformer;
import cz.cvut.fel.omo.foodChain.operations.*;

import java.util.ArrayList;
import java.util.List;

import static cz.cvut.fel.omo.foodChain.operations.OperationEnum.*;

/**
 * The class describes the general state and behavior of a farmer.
 *
 * @author Artem Hurbych, Pavel Paklonski
 */
public abstract class Farmer extends ChainParticipant {

    private String country;
    private int age;
    private List<FoodEnum> ingredientList;
    private List<FoodEntity> products = new ArrayList<>();
    private OperationEnum operationType = GROW;

    public Farmer(String name, Integer age, String country, int cashAccount, List<FoodEnum> ingredientList, TransactionInformer transactionInformer) {
        super(name, cashAccount, transactionInformer);

        if(age == null) throw new IllegalArgumentException("Age can't be null!");
        if(age < 16) throw new IllegalArgumentException("Age must be 16 or more!");
        if(age > 130) throw new IllegalArgumentException("Age must be 130 or less!");
        this.age = age;

        if(country == null) throw new IllegalArgumentException("Country can't be null!");
        this.country = country;

        if(ingredientList == null) throw new IllegalArgumentException("Ingredient list can't be null!");
        this.ingredientList = ingredientList;
    }

    public String getCountry() {
        return country;
    }

    public int getAge() {
        return age;
    }

    public OperationEnum getOperationType() {
        return operationType;
    }

    /**
     * Check if the farmer can execute the request.
     *
     * @param request the instance of Request
     * @return true - if the farmer can execute the request
     */
    public boolean isAgreeToExecute(Request request) {
        return ingredientList.contains(request.getFoodEnum());
    }

    public abstract FoodEntity process(Request request) throws UnexpectedFoodEntityException, MealWasNotCookedException;

    /**
     * Add the entity of food to the list of products.
     *
     * @param foodEntity the instance of food that was produced by someone
     */
    public void addFood(FoodEntity foodEntity) {
        products.add(foodEntity);
    }

    public List<FoodEntity> getProducts() {
        return products;
    }

}
