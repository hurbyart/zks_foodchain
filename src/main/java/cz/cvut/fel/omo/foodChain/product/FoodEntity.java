package cz.cvut.fel.omo.foodChain.product;

import cz.cvut.fel.omo.foodChain.chainParticipants.Party;
import cz.cvut.fel.omo.foodChain.states.State;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import static cz.cvut.fel.omo.foodChain.simulation.Main.LOGGER;


public class FoodEntity {


    private FoodEnum name;
    private int quantity;
    private int quality;
    private final int storageTemperature;
    private List<String> doneActions = new ArrayList<>();

    private FoodEntityStateMachine stateMachine;

    public FoodEntity(FoodEnum name, Integer quantity, Integer storageTemperature, Integer quality) {
        if(name == null) throw new IllegalArgumentException("Name can't be null!");
        this.name = name;

        if(quantity == null) throw new IllegalArgumentException("Quantity can't be null!");
        if(quantity <= 0) throw new IllegalArgumentException("Quantity must be more then 0!");
        if(quantity > 50) throw new IllegalArgumentException("Quantity must be 50 or less!");

        this.quantity = quantity;
        if(quality == null) throw new IllegalArgumentException("Quality can't be null!");
        if(quality <= 0) throw new IllegalArgumentException("Quality must be more then 0!");
        if(quality > 100) throw new IllegalArgumentException("Quality must be 100 or less!");
        this.quality = quality;

        if(storageTemperature == null) throw new IllegalArgumentException("Storage temperature can't be null!");
        if(storageTemperature < -20) throw new IllegalArgumentException("Storage temperature must be -20 or more!");
        if(storageTemperature > 10) throw new IllegalArgumentException("Storage temperature must be 10 or less!");
        this.storageTemperature = storageTemperature;
        this.stateMachine = new FoodEntityStateMachine(this);
    }

    public FoodEnum getName() {
        return name;
    }

    public int getQuality() {
        return quality;
    }

    public int getQuantity() {
        return quantity;
    }

    public int getStorageTemperature() {
        return storageTemperature;
    }

    public List<String> getDoneActions() {
        return doneActions;
    }

    public void addAction(String action) {
        doneActions.add(action);
    }

    /**
     * uses for product report
     */
    public void writeAllActions() {
        for (String action : doneActions) {
            System.out.println(action);
        }
    }

    public void writeAllActionsText() {
        try (Writer writer = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(this + "Report.txt"), "utf-8"))) {
            writer.write("-------------------- FOODENTITY INFORMER REPORT --------------------\n");
            for (String action : doneActions) {
                writer.write(action + "\n");
            }
        } catch (IOException e) {
            LOGGER.warning(e.getMessage());
        }
    }

    /**
     * uses for change food entity state to unpacked
     */
    public void useForCooking() {
        stateMachine.useForCooking();
    }

    /**
     * if applicant is customer food entity will be packed in final packing,otherwise in temporary packing
     *
     * @param applicant
     */
    public void transport(Party applicant) {
        stateMachine.transport(applicant);
    }

    public State getState() {
        return stateMachine.getState();
    }

    public void setState(State state) {
        stateMachine.setState(state);
    }
}
