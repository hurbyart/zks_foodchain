package cz.cvut.fel.omo.foodChain.exceptions;

public class UnexpectedFoodEntityException extends Exception{

    public UnexpectedFoodEntityException() {
        super("Unexpected food entity!");
    }

}
