package cz.cvut.fel.omo.foodChain.chainParticipants;

import cz.cvut.fel.omo.foodChain.exceptions.MealWasNotCookedException;
import cz.cvut.fel.omo.foodChain.exceptions.UnexpectedFoodEntityException;
import cz.cvut.fel.omo.foodChain.exceptions.UnexpectedRequestSpecializationForManufacturerException;
import cz.cvut.fel.omo.foodChain.visitor.Visitor;
import cz.cvut.fel.omo.foodChain.product.FoodEntity;
import cz.cvut.fel.omo.foodChain.product.FoodEnum;
import cz.cvut.fel.omo.foodChain.operations.OperationEnum;
import cz.cvut.fel.omo.foodChain.operations.Request;
import cz.cvut.fel.omo.foodChain.operations.Transaction;

import java.util.HashMap;

public interface Party {

    HashMap<FoodEnum, Integer> getPrices();

    OperationEnum getOperationType();

    boolean isAgreeToExecute(Request request);

    FoodEntity process(Request request) throws UnexpectedFoodEntityException, MealWasNotCookedException, UnexpectedRequestSpecializationForManufacturerException;

    void addFood(FoodEntity foodEntity);

    void registerToTheChannel();

    void updateTransactions(Transaction transaction);

    String getName();

    boolean isAgreeToPay(int price);

    void payMoney(int price);

    void getProfit();

    int askPrice(FoodEnum foodEnum, int quantity);

    int getCashAccount();

    int getLastHash();

    void accept(Visitor visitor);
}