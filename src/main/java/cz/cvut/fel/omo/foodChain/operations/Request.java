package cz.cvut.fel.omo.foodChain.operations;

import cz.cvut.fel.omo.foodChain.product.FoodEnum;
import cz.cvut.fel.omo.foodChain.chainParticipants.Party;

public class Request {

    private Party applicant;
    private FoodEnum foodEnum;
    private Integer quantity;
    private OperationEnum operationType;
    private int price;
    public boolean done = false;

    public Request(Party applicant, FoodEnum foodEnum, Integer quantity, OperationEnum operationType) {
        this.applicant = applicant;

        if(foodEnum == null) throw new IllegalArgumentException("Food enum cannot be null!");
        this.foodEnum = foodEnum;

        if(quantity == null) throw new IllegalArgumentException("Quantity cannot be null!");
        if(quantity <= 0) throw new IllegalArgumentException("Quantity must be 1 or more!");
        if(quantity > 50) throw new IllegalArgumentException("Quantity must be 50 or less!");
        this.quantity = quantity;

        if(operationType == null) throw new IllegalArgumentException("Operation type cannot be null!");
        this.operationType = operationType;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public Party getApplicant() {
        return applicant;
    }

    public FoodEnum getFoodEnum() {
        return foodEnum;
    }

    public int getQuantity() {
        return quantity;
    }

    public OperationEnum getOperationType() {
        return operationType;
    }

    @Override
    public String toString() {
        return "REQUEST: " + "applicant = " + applicant +
                ", foodEnum = " + foodEnum + ", quantity = " + quantity +
                ", operationType = " + operationType + '.';
    }

    public void showRequest(){
        System.out.println(toString());
    }
}
