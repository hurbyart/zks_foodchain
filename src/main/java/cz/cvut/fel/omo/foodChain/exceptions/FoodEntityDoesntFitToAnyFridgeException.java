package cz.cvut.fel.omo.foodChain.exceptions;

public class FoodEntityDoesntFitToAnyFridgeException extends Exception {

    public FoodEntityDoesntFitToAnyFridgeException() {
        super("Food entity doesn't fit to any fridge!");
    }
}

