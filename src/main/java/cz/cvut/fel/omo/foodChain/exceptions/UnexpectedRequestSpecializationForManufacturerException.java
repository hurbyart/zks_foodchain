package cz.cvut.fel.omo.foodChain.exceptions;

public class UnexpectedRequestSpecializationForManufacturerException extends Exception{

    public UnexpectedRequestSpecializationForManufacturerException() {
        super("Unexpected request specialization for manufacturer!");
    }

}

